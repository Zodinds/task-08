package com.epam.rd.java.basic.task8;

public class Flower {
    String name;
    String soil;
    String origin;
    String stemColour;
    String leafColour;
    int aveLenFlower;
    String aveLenFlowerMeasurement;
    int temperature;
    String temperatureMeasurement;
    String lightRequirement;
    int watering;
    String wateringMeasurement;
    String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getAveLenFlowerMeasurement() {
        return aveLenFlowerMeasurement;
    }

    public void setAveLenFlowerMeasurement(String aveLenFlowerMeasurement) {
        this.aveLenFlowerMeasurement = aveLenFlowerMeasurement;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getTemperatureMeasurement() {
        return temperatureMeasurement;
    }

    public void setTemperatureMeasurement(String temperatureMeasurement) {
        this.temperatureMeasurement = temperatureMeasurement;
    }

    public String getLightRequirement() {
        return lightRequirement;
    }

    public void setLightRequirement(String lightRequirement) {
        this.lightRequirement = lightRequirement;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getWateringMeasurement() {
        return wateringMeasurement;
    }

    public void setWateringMeasurement(String wateringMeasurement) {
        this.wateringMeasurement = wateringMeasurement;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}
