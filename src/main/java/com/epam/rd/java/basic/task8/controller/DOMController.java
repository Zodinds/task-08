package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getContainer(){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		List<Flower> flowerList = new ArrayList<>();

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(xmlFileName);
			NodeList flowers = document.getElementsByTagName("flower");

			for(int i=0; i< flowers.getLength(); i++){
				Flower flowerToAdd = new Flower();
				Node flower = flowers.item(i);
				if(flower.getNodeType() == Node.ELEMENT_NODE) {
					NodeList flowerCharacteristics = flower.getChildNodes();
					flowerToAdd.setName(flowerCharacteristics.item(1).getTextContent());
					flowerToAdd.setSoil(flowerCharacteristics.item(3).getTextContent());
					flowerToAdd.setOrigin(flowerCharacteristics.item(5).getTextContent());
					flowerToAdd.setMultiplying(flowerCharacteristics.item(11).getTextContent());

					NodeList visualParameters = flowerCharacteristics.item(7).getChildNodes();
					flowerToAdd.setStemColour(visualParameters.item(1).getTextContent());
					flowerToAdd.setLeafColour(visualParameters.item(3).getTextContent());
					flowerToAdd.setAveLenFlowerMeasurement(visualParameters.item(5)
							.getAttributes().item(0).getTextContent());
					flowerToAdd.setAveLenFlower(Integer.parseInt(visualParameters.item(5).getTextContent()));

					NodeList growingTips = flowerCharacteristics.item(9).getChildNodes();
					flowerToAdd.setTemperature(Integer.parseInt(growingTips.item(1).getTextContent()));
					flowerToAdd.setTemperatureMeasurement(growingTips.item(1)
							.getAttributes().item(0).getTextContent());
					flowerToAdd.setLightRequirement(growingTips.item(3)
							.getAttributes().item(0).getTextContent());
					flowerToAdd.setWatering(Integer.parseInt(growingTips.item(5).getTextContent()));
					flowerToAdd.setWateringMeasurement(growingTips.item(5)
							.getAttributes().item(0).getTextContent());
				}
				flowerList.add(flowerToAdd);
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowerList;
	}

	public void sortFlowersByName(List<Flower> flowerList){
		flowerList.sort((Flower f1, Flower f2)-> f1.getName().compareToIgnoreCase(f2.getName()));
	}

	public void outputFlower(List<Flower> flowerList,String path){
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter streamWriter;
		try(FileWriter fileWriter = new FileWriter(path)){
			streamWriter = outputFactory.createXMLStreamWriter(fileWriter);
			streamWriter.writeStartDocument("UTF-8", "1.0");
			streamWriter.writeCharacters("\n");

			streamWriter.writeStartElement("flowers");
			streamWriter.writeAttribute("xmlns", "http://www.nure.ua");
			streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			streamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
			streamWriter.writeCharacters("\n");

			for(Flower flower: flowerList) {
				streamWriter.writeStartElement("flower");
				streamWriter.writeCharacters("\n");
				streamWriter.writeStartElement("name");
				streamWriter.writeCharacters(flower.getName());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("soil");
				streamWriter.writeCharacters(flower.getSoil());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("origin");
				streamWriter.writeCharacters(flower.getOrigin());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("visualParameters");
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("stemColour");
				streamWriter.writeCharacters(flower.getStemColour());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("leafColour");
				streamWriter.writeCharacters(flower.getLeafColour());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("aveLenFlower");
				streamWriter.writeAttribute("measure", flower.getAveLenFlowerMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getAveLenFlower()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("growingTips");
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("temperature");
				streamWriter.writeAttribute("measure", flower.getTemperatureMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getTemperature()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEmptyElement("lighting");
				streamWriter.writeAttribute("lightRequiring", flower.getLightRequirement());
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("watering");
				streamWriter.writeAttribute("measure", flower.getWateringMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getWatering()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("multiplying");
				streamWriter.writeCharacters(flower.getMultiplying());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

			}
			streamWriter.writeEndElement();

		}catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
	}

}
