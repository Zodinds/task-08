package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		List<Flower> flowerList;
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowerList = domController.getContainer();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		domController.sortFlowersByName(flowerList);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.outputFlower(flowerList, outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowerList = saxController.getContainer();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		saxController.sortFlowersByAveLenFlower(flowerList);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.outputFlower(flowerList, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowerList = staxController.getContainer();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		staxController.sortFlowersByWatering(flowerList);
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.outputFlower(flowerList, outputXmlFile);
	}

}
