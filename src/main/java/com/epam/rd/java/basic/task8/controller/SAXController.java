package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private final String xmlFileName;
	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getContainer(){
		SAXParserFactory factory = SAXParserFactory.newInstance();

		List<Flower> flowerList = new ArrayList<>();
		try {
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler(){
				boolean flower, name, soil, origin, stemColour, leafColour,
						aveLenFlower, temperature, watering, multiplying;

				@Override
				public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
					if(qName.equals("flower")) flower=true;
					if(qName.equals("name")) name=true;
					if(qName.equals("soil")) soil=true;
					if(qName.equals("origin")) origin=true;
					if(qName.equals("stemColour")) stemColour=true;
					if(qName.equals("leafColour")) leafColour=true;
					if(qName.equals("aveLenFlower")) {
						flowerList.get(flowerList.size()-1).setAveLenFlowerMeasurement(attributes.getValue(0));
						aveLenFlower = true;
					}
					if(qName.equals("temperature")) {
						flowerList.get(flowerList.size()-1).setTemperatureMeasurement(attributes.getValue(0));
						temperature = true;
					}
					if(qName.equals("lighting")) {
						flowerList.get(flowerList.size()-1).setLightRequirement(attributes.getValue(0));
					}
					if(qName.equals("watering")) {
						flowerList.get(flowerList.size()-1).setWateringMeasurement(attributes.getValue(0));
						watering = true;
					}
					if(qName.equals("multiplying")) multiplying=true;
				}

				@Override
				public void characters(char[] ch, int start, int length) throws SAXException {
					if(flower){
						flowerList.add(new Flower());
						flower = false;
					} if(name){
						flowerList.get(flowerList.size()-1).setName(new String(ch, start, length));
						name = false;
					} if(soil){
						flowerList.get(flowerList.size()-1).setSoil(new String(ch, start, length));
						soil = false;
					} if(origin){
						flowerList.get(flowerList.size()-1).setOrigin(new String(ch, start, length));
						origin = false;
					}if(stemColour) {
						flowerList.get(flowerList.size()-1).setStemColour(new String(ch, start, length));
						stemColour = false;
					}if(leafColour) {
						flowerList.get(flowerList.size()-1).setLeafColour(new String(ch, start, length));
						leafColour = false;
					} if(aveLenFlower) {
						flowerList.get(flowerList.size()-1).setAveLenFlower(Integer.parseInt(new String(ch, start, length)));
						aveLenFlower = false;
					} if(temperature){
						flowerList.get(flowerList.size()-1).setTemperature(Integer.parseInt(new String(ch, start, length)));
						temperature = false;
					} if(watering){
						flowerList.get(flowerList.size()-1).setWatering(Integer.parseInt(new String(ch, start, length)));
						watering = false;
					} if(multiplying){
						flowerList.get(flowerList.size()-1).setMultiplying(new String(ch, start, length));
						multiplying = false;
					}
				}
			};
			saxParser.parse(xmlFileName, handler);
		}catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowerList;
	}

	public void sortFlowersByAveLenFlower(List<Flower> flowerList){
		flowerList.sort(Comparator.comparingInt(Flower::getAveLenFlower));
	}

	public void outputFlower(List<Flower> flowerList,String path){
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter streamWriter;
		try(FileWriter fileWriter = new FileWriter(path)){
			streamWriter = outputFactory.createXMLStreamWriter(fileWriter);
			streamWriter.writeStartDocument("UTF-8", "1.0");
			streamWriter.writeCharacters("\n");

			streamWriter.writeStartElement("flowers");
			streamWriter.writeAttribute("xmlns", "http://www.nure.ua");
			streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			streamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
			streamWriter.writeCharacters("\n");

			for(Flower flower: flowerList) {
				streamWriter.writeStartElement("flower");
				streamWriter.writeCharacters("\n");
				streamWriter.writeStartElement("name");
				streamWriter.writeCharacters(flower.getName());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("soil");
				streamWriter.writeCharacters(flower.getSoil());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("origin");
				streamWriter.writeCharacters(flower.getOrigin());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("visualParameters");
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("stemColour");
				streamWriter.writeCharacters(flower.getStemColour());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("leafColour");
				streamWriter.writeCharacters(flower.getLeafColour());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("aveLenFlower");
				streamWriter.writeAttribute("measure", flower.getAveLenFlowerMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getAveLenFlower()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("growingTips");
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("temperature");
				streamWriter.writeAttribute("measure", flower.getTemperatureMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getTemperature()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEmptyElement("lighting");
				streamWriter.writeAttribute("lightRequiring", flower.getLightRequirement());
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("watering");
				streamWriter.writeAttribute("measure", flower.getWateringMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getWatering()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("multiplying");
				streamWriter.writeCharacters(flower.getMultiplying());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

			}
			streamWriter.writeEndElement();

		}catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
	}
}