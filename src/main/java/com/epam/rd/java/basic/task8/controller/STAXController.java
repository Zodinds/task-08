package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getContainer() {
		List<Flower> flowerList = new ArrayList<>();
		try {
			XMLEventReader reader = XMLInputFactory.newInstance().createXMLEventReader(new FileInputStream(xmlFileName));
			while(reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if(event.isStartElement()){
					StartElement startElement = event.asStartElement();
					switch (startElement.getName().getLocalPart()){
						case "flower":
							flowerList.add(new Flower());
							break;
						case "name":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1).setName(event.asCharacters().getData());
							break;
						case "soil":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1).setSoil(event.asCharacters().getData());
							break;
						case "origin":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1).setOrigin(event.asCharacters().getData());
							break;
						case "stemColour":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1).setStemColour(event.asCharacters().getData());
							break;
						case "leafColour":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1).setLeafColour(event.asCharacters().getData());
							break;
						case "aveLenFlower":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1)
									.setAveLenFlower(Integer.parseInt(event.asCharacters().getData()));
							Attribute measure = startElement.getAttributeByName(new QName("measure"));
							flowerList.get(flowerList.size()-1).setAveLenFlowerMeasurement(measure.getValue());
							break;
						case "temperature":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1)
									.setTemperature(Integer.parseInt(event.asCharacters().getData()));
							measure = startElement.getAttributeByName(new QName("measure"));
							flowerList.get(flowerList.size()-1).setTemperatureMeasurement(measure.getValue());
							break;
						case "lighting":
							Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
							flowerList.get(flowerList.size()-1).setLightRequirement(lightRequiring.getValue());
							break;
						case "watering":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1)
									.setWatering(Integer.parseInt(event.asCharacters().getData()));
							measure = startElement.getAttributeByName(new QName("measure"));
							flowerList.get(flowerList.size()-1).setWateringMeasurement(measure.getValue());
							break;
						case "multiplying":
							event = reader.nextEvent();
							flowerList.get(flowerList.size()-1).setMultiplying(event.asCharacters().getData());
							break;

					}
				}
			}

		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}

		return flowerList;
	}

	public void sortFlowersByWatering(List<Flower> flowerList){
		flowerList.sort(Comparator.comparingInt(Flower::getWatering));
	}

	public void outputFlower(List<Flower> flowerList,String path){
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter streamWriter;
		try(FileWriter fileWriter = new FileWriter(path)){
			streamWriter = outputFactory.createXMLStreamWriter(fileWriter);
			streamWriter.writeStartDocument("UTF-8", "1.0");
			streamWriter.writeCharacters("\n");

			streamWriter.writeStartElement("flowers");
			streamWriter.writeAttribute("xmlns", "http://www.nure.ua");
			streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			streamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
			streamWriter.writeCharacters("\n");

			for(Flower flower: flowerList) {
				streamWriter.writeStartElement("flower");
				streamWriter.writeCharacters("\n");
				streamWriter.writeStartElement("name");
				streamWriter.writeCharacters(flower.getName());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("soil");
				streamWriter.writeCharacters(flower.getSoil());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("origin");
				streamWriter.writeCharacters(flower.getOrigin());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("visualParameters");
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("stemColour");
				streamWriter.writeCharacters(flower.getStemColour());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("leafColour");
				streamWriter.writeCharacters(flower.getLeafColour());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("aveLenFlower");
				streamWriter.writeAttribute("measure", flower.getAveLenFlowerMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getAveLenFlower()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("growingTips");
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("temperature");
				streamWriter.writeAttribute("measure", flower.getTemperatureMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getTemperature()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEmptyElement("lighting");
				streamWriter.writeAttribute("lightRequiring", flower.getLightRequirement());
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("watering");
				streamWriter.writeAttribute("measure", flower.getWateringMeasurement());
				streamWriter.writeCharacters(Integer.toString(flower.getWatering()));
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeStartElement("multiplying");
				streamWriter.writeCharacters(flower.getMultiplying());
				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

				streamWriter.writeEndElement();
				streamWriter.writeCharacters("\n");

			}
			streamWriter.writeEndElement();

		}catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
	}

}